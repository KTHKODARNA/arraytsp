import java.util.*;
import java.lang.Math;

public class Main {
	Kattio io = new Kattio(System.in, System.out);
	int numberOfCities;
	int numberOfNeighbors = 20;
	double[][] distances;
	int[][] neighbors;
	int[] positions;
	static long start;
	static long limit = 1300L;
	boolean improved;

	public static void main(String[] args) {
		start = System.currentTimeMillis();
		Main m = new Main();
		m.run();
	}

	public void Main() {

	}

	public void run() {
		parseInput();
		storeNeighbors();
		int[] tour = kattisGreedy();
		improved = true;
		while(improved) {
			if(System.currentTimeMillis() - start > limit) {
				break;
			} else {
				improved = false;
				tour = twoOpt(tour);
			}
		}
		printTour(tour);
	}

	public void parseInput() {
		numberOfCities = io.getInt();
		Node[] coordinates = new Node[numberOfCities];
		positions = new int[numberOfCities];
		if(numberOfCities < numberOfNeighbors){
			numberOfNeighbors = numberOfCities;
		}
		int count = 0;
		while(io.hasMoreTokens()) {
			double x = io.getDouble();
			double y = io.getDouble();
			coordinates[count] = new Node(x, y);
			count++;
		}
		preCalc(coordinates);

	}

	public void preCalc(Node[] coordinates) {
		distances = new double[numberOfCities][numberOfCities];
		for(int i=0; i<numberOfCities; i++) {
			for(int j=i+1; j<numberOfCities; j++) {
				if(i==j) { continue; }

				double dist = getDistanceBetween(i, j, coordinates);
				distances[i][j] = dist;
				distances[j][i] = dist;
			}
		}
	}

	public double getDistanceBetween(int i, int j, Node[] coordinates) {
		
		double first = ((coordinates[j].getX())-(coordinates[i].getX()))*((coordinates[j].getX())-(coordinates[i].getX()));
		double second = ((coordinates[j].getY())-(coordinates[i].getY()))*((coordinates[j].getY())-(coordinates[i].getY()));
		return Math.sqrt(first+second);
	}

	public void storeNeighbors() {
		neighbors = new int[numberOfCities][numberOfNeighbors];
		for(int i=0; i<numberOfCities; i++) {
			boolean[] visited = new boolean[numberOfCities];
			neighbors[i][0] = i;
			visited[i] = true;
			int best = -1;
			double shortestDistance = Double.MAX_VALUE;
			int count = 1;
			while(count < numberOfNeighbors) {
				for(int j=0; j<numberOfCities; j++) {
					if(i==j) { continue; } // Kan vara onÃ¶dig
					if(!visited[j]) {
						if(best == -1 || distances[i][j] < shortestDistance) {
							shortestDistance = distances[i][j];
							best = j;
						}
					}
				}
				neighbors[i][count] = best;
				visited[best] = true;
				shortestDistance = Double.MAX_VALUE;
				count++;
			}
		}
	}

	public int[] kattisGreedy() {
		int[] tour = new int[numberOfCities];
		boolean[] visited = new boolean[numberOfCities];
		tour[0] = 0;
		//coordinates[0].setPosition(0);
		positions[0] = 0;
		visited[0] = true;
		for(int i=1; i<numberOfCities; i++) {
			int best = -1;
			for(int j=1; j<numberOfNeighbors; j++) {
				int candidate = neighbors[i-1][j];
				if(!visited[candidate]) {
					if(best == -1 || distances[i-1][candidate] < distances[i-1][best]) {
						best = candidate;
					}
				}
			}
			if(best == -1) {
				for(int j=1; j<numberOfCities; j++) {
					if(!visited[j]) {
						if(best == -1 || distances[i-1][j] < distances[i-1][best]) {
							best = j;
						}
					}
				}
			}
			tour[i] = best;
			//coordinates[best].setPosition(i);
			positions[best] = i;
			visited[best] = true;
		}
		return tour;
	}

	public void printTour(int[] tour) {
		for(int i : tour) {
			io.println(i);
		}
		io.close();
	}

	public int[] twoOptSwap(int[] tour, int j, int k) {
		if(j > k) {
			return twoOptSwap(tour, k, j);
		}
		int[] tmpTour = new int[numberOfCities];
		if(System.currentTimeMillis() - start > limit) {
			return tour;
		}
		for(int i=0; i<=j; i++) {
			tmpTour[i] = tour[i];
		}
		int continueFrom = j+1;

		for(int i=k; i>j; i--) {
			tmpTour[continueFrom] = tour[i];
			//coordinates[tour[i]].setPosition(continueFrom);
			positions[tour[i]] = continueFrom;
			continueFrom++;
		}

		for(int i=k+1; i<numberOfCities; i++) {
			tmpTour[i] = tour[i];
		}
		return tmpTour;
	}

	public int[] twoOpt(int[] tour) {

		for(int i=0; i<tour.length; i++) {
			int t1 = tour[i];
			for(int j=1; j<numberOfNeighbors; j++) {
				int t2 = tour[0];
				if(i != tour.length-1) {
					t2 = tour[i+1];
				}
				//int neighborsPos = coordinates[neighbors[tour[i]][j]].getPosition();
				int neighborsPos = positions[neighbors[tour[i]][j]];
				if(Math.abs(i-neighborsPos) <= 1) continue;

				int t4 = tour[neighborsPos];
				int t3 = tour[0];
				if(neighborsPos != tour.length-1) {
					t3 = tour[neighborsPos+1];
				}

				double old_cost = distances[t1][t2] + distances[t3][t4];
				double new_cost = distances[t1][t4] + distances[t2][t3];
				if(new_cost < old_cost) {
					//System.err.println("Found a swap");
					tour = twoOptSwap(tour, i, neighborsPos);
					improved = true;
					//System.err.println(getDistanceOf(tour));
				}
			}
		}
		return tour;
	}

	public double getDistanceOf(int[] tour) {
		double distance = 0;
		for(int i=0; i<tour.length; i++) {
			if((i+1) == tour.length) {
				return distance += distances[tour[0]][tour[i]];
			}
			distance += distances[tour[i]][tour[i+1]];
		}
		return distance;
	}

}