public class Node {
	private double x, y;
	
	public Node(double xin, double yin) {
		x = xin; 
		y = yin;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}